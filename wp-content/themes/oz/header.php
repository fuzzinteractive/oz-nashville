<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * @package _s
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link href='http://fonts.googleapis.com/css?family=Raleway:400,300,500,600,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Poly:400,400italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/library/css/style.css">
<script type="text/javascript">var switchTo5x=true;</script>
<script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "9e3cce42-9afe-43cf-9f4e-8333561455b9", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php do_action( 'before' ); ?>
	<div class="site-wrap group">
	<!--Serve up top nav except for front page-->
	<?php 
		if(!is_front_page()) {
	?>		
	<div class="top-nav-contain">
		<div class="row">
			<div class="logo-contain large-2 columns">
				<nav class="burger">
					<a id="sidr-trig" href="#" title="access main nav" class="burger-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/burger.svg" alt="Acess Main Nav"></a>
					<!--<a id="sidr-trig" href="#" title="access main nav" class="burger-icon">&#9776;</a>-->
				</nav>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img class="site-logo" src="/wp-content/themes/oz/library/images/logo_oz_black.jpg" alt="Oz Nashville Arts Museum" /></a>
			</div>
			<nav class= "columns large-7 primary-nav">
				<?php wp_nav_menu( array('menu' => 'Primary' )); ?>
			</nav>
		</div>
	</div>	
	<?php } ?>