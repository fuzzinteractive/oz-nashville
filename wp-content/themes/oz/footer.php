<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package _s
 */
?>

	<footer class="colophon">
		
				<h3>Contact OZ</h3>
				<p>ph: <a href="tel:6153507200">(615) 350-7200</a><br>
					<a href="mailto:hello@oznashville.com">hello@oznashville.com</a><br>
					<span class="address">6172 Cockrill Bend Circle<br>
					Nashville, TN 37209</span>
					<!--<span class="foot-social"><a href="https://www.facebook.com/OZNashville" target="_blank"><img src="/wp-content/themes/oz/library/images/facebook.png" alt="OZ Nashville on Facebook"></a> | <a href="http://instagram.com/oznashville/" target = "_blank"><img src="/wp-content/themes/oz/library/images/instagram.png" alt="OZ Nashville on Instagram"></a></span>-->
					<span class="foot-social">
						<a class="foot-fb1" href="https://www.facebook.com/OZNashville" target="_blank"></a>
						<a class="foot-fb2" href="https://www.facebook.com/bravenewart" target="_blank"></a>
						<a class="foot-twitter" href="https://www.twitter.com/OZNashville" target="_blank"></a>
						<a class="foot-instagram" href="http://instagram.com/oznashville" target="_blank"></a>
						<a class="foot-youtube" href="https://www.facebook.com/OZNashville" target="_blank"></a>


					</span>
				</p>
	</footer>
</div><!--site-wrap-->
<?php wp_footer(); ?>
<?php wp_nav_menu( array('menu' => 'Small Device Menu', 'container' => 'div', 'container_id' => 'sidr')); ?>
</body>
</html>