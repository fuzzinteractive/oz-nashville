<?php
/**
 * The template for displaying Archive 
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package _s
 */

get_header(); ?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<div class="row full-row title-contain">
			<div class="large-6 columns">
				<h1 class="dots">CALENDAR</h1>
			</div>
			<nav class="large-2 columns view-switch">
				<span class="icon-contain">
					<a href="/programs">
						<svg version="1.1" class="nav-icon" id="grid-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
						<rect fill="#ffffff" width="27" height="27"/>
						<rect x="37" fill="#ffffff" width="26" height="27"/>
						<rect x="73" fill="#ffffff" width="27" height="27"/>
						<rect y="37" fill="#ffffff" width="27" height="26"/>
						<rect x="37" y="37" fill="#ffffff" width="26" height="26"/>
						<rect x="73" y="37" fill="#ffffff" width="27" height="26"/>
						<rect y="73" fill="#ffffff" width="27" height="27"/>
						<rect x="37" y="73" fill="#ffffff" width="26" height="27"/>
						<rect x="73" y="73" fill="#ffffff" width="27" height="27"/>
						</svg>
					</a>
				</span>
				<span class="icon-contain">
					<a href="/program-list">
						<svg version="1.1" class="nav-icon" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
						<rect fill="#ffffff" width="27" height="27"/>
						<rect x="37" fill="#ffffff" width="50" height="27"/>
						<rect x="73" fill="#ffffff" width="27" height="27"/>
						<rect y="37" fill="#ffffff" width="27" height="26"/>
						<rect x="37" y="37" fill="#ffffff" width="55" height="26"/>
						<rect x="73" y="37" fill="#ffffff" width="27" height="26"/>
						<rect y="73" fill="#ffffff" width="27" height="27"/>
						<rect x="37" y="73" fill="#ffffff" width="53" height="27"/>
						<rect x="73" y="73" fill="#ffffff" width="27" height="27"/>
						</svg>
					</a>
				</span>
			</nav>
		</div>
		<?php if ( have_posts() ) : ?>


			<?php /* Start the Loop */ ?>
			<?php while ( have_posts() ) : the_post(); ?>

				<?php
					/* Include the Post-Format-specific template for the content.
					 * If you want to override this in a child theme then include a file
					 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
					 */
					get_template_part( 'content', 'calendar' );
				?>

			<?php endwhile; ?>

			<?php _s_content_nav( 'nav-below' ); ?>

		<?php else : ?>

			<?php get_template_part( 'no-results', 'archive' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

<?php get_footer(); ?>
