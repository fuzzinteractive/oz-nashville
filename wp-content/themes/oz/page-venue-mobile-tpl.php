<?php
/**
 * Template Name: Venue Mobile Page
 *
 * different template.
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<!-- OVERVIEW hardcoded - to be replaced with a wp page -->
		<div class="venue overview">
			<div class="row">
				<div class="columns large-8 large-centered">
					<h1 class="entry-title">OVERVIEW</h1>
					<!--<nav class="venue-sub"><a class="pink-text" href="#">Special Events</a> / <a class="blue-text" href="#">Weddings</a> / <a class ="orange-text" href="#">Corporate</a></nav>-->
					<p>OZ features five distinctly unique areas for truly memorable occasions.  From the dramatic Ultra Lounge Patio with retractable glass doors, majestic views, and romantic fire pit area, to the intimately peaceful Zen Garden with exotic foliage–OZ contains one-of-a-kind spaces to create unforgettable moments.  The venue also boasts one of the world’s largest walk-in cigar humidors, The Escaparate, which held over 100,000 premium cigars.  A 12,000 square-foot warehouse space provides a sizable canvas to create the original environment of your choosing.</p>
					<p>OZ prides itself on its staff of creative, professional, and customer-friendly people.  Our team will provide you with attentive, one-on-one service and has a variety of professional resources that will be at your disposal.  Our goal is to assist you in creating the occasion of your dreams and one you will cherish for the rest of your life.</p>
					<p><a href="/contact">Contact us</a> to arrange a private tour to view the space and imagine the possibilities</p>
				</div>
			</div>
		</div>
		<!--<img class="scroll-guy" src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-guy.png" alt="scroll down">-->




			<?php $args = array( 'post_type' => 'venues', 'posts_per_page' => 10, 'order'=> 'asc' );
			$loop = new WP_Query( $args );
			$i = 0;
			while ( $loop->have_posts() ) : $loop->the_post();
				$i++;
				if($i == 1 || $i == 5) {
					$color = "green";
					$dmd ="green-dmd";
				} elseif($i == 2 || $i == 6) {
					$color = "blue";
					$dmd ="blue-dmd";
				}
				elseif($i == 3 || $i == 7) {
					$color = "orange";
					$dmd ="orange-dmd";
				}
				else {
					$color = "pink";
					$dmd ="pink-dmd";
				} ?>

				<div class="venue <?php echo $color; ?>" id="venue-<?php echo $i ?>">


					
					<div class="venue-info-contain" id="anchor-<?php echo $i; ?>">
						<div class="row">
							<div class="large-4 large-offset-1 columns">
								<h2 class="v-title"><?php the_title(); ?></h2>

								<?php the_content(); ?>
							</div>

							<div class="large-12 slide-contain columns">
								<div class="cycle-slideshow" 
								    data-cycle-fx=scrollHorz
								    data-cycle-timeout=5000
								    data-cycle-swipe=true
								    data-cycle-paused=true
								    data-cycle-pager=".pager-<?php echo $i; ?>"
								    >
							
								    <div class="cycle-pager"></div>
								    <?php $images = get_field('venue_gallery'); 
								     foreach( $images as $image ): ?>
								   	 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								 	<?php endforeach; ?>
								</div>
								<div class="pager-<?php echo $i; ?> example-pager"></div>

							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
	</div><!-- #primary -->

	<!--<nav class="venue-path">
		<li><a  href="#top">Overview</a></li>
		<li><a  href="#anchor-1">Ozgener Gallery</a></li>
		<li><a  href="#anchor-2">Grand Salon</a></li>
		<li><a href="#anchor-3">Ultra Lounge</a></li>
		<li><a  href="#anchor-4">Patio Contempo</a></li>
		<li><a  href="#anchor-5">Escaparten</a></li>
		<li><a  href="#anchor-6">Zen Garden</a></li>
		<li><a  href="#anchor-7">Boardroom</a></li>
	</nav>-->


<?php get_footer(); ?>
