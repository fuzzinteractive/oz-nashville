<?php
/**
 * Template Name: Patronage Pagee
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'patronage' ); ?>


			<?php endwhile; // end of the loop. ?>

	</div><!-- #primary -->

<?php get_footer(); ?>
