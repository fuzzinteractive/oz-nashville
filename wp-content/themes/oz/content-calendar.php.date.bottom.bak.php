<?php
/**
 * @package _s
 */
?>



<article id="post-<?php the_ID(); ?>" class="large-3 columns calendar-item <?php the_field('background_color'); ?>">
		

	<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="cal-bkg-contain">
		<?php the_field('calendar_view_title'); ?>
		<span href="<?php the_permalink(); ?>" title="<?php the_title(); ?>" class="cal-bkg">
		</span>
		<?php if(has_post_thumbnail() ) {
			the_post_thumbnail('calendar-thumb');
		} ?>
	</a>
	<span class="cal-item-month">
		<?php the_field('month'); ?>
	</span>
		<span class="cal-item-day">
		<?php the_field('display_day'); ?>
	</span>



</article><!-- #post-## -->
