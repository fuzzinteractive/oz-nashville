<?php
/**
 * _s functions and definitions
 *
 * @package _s
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) )
	$content_width = 640; /* pixels */

if ( ! function_exists( '_s_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function _s_setup() {

	/**
	 * Make theme available for translation
	 * Translations can be filed in the /languages/ directory
	 * If you're building a theme based on _s, use a find and replace
	 * to change '_s' to the name of your theme in all the template files
	 */
	load_theme_textdomain( '_s', get_template_directory() . '/languages' );

	/**
	 * Add default posts and comments RSS feed links to head
	 */
	add_theme_support( 'automatic-feed-links' );

	/**
	 * Enable support for Post Thumbnails on posts and pages
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	/**
	 * This theme uses wp_nav_menu() in one location.
	 */
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', '_s' ),
	) );

	/**
	 * Enable support for Post Formats
	 */
	add_theme_support( 'post-formats', array( 'aside', 'image', 'video', 'quote', 'link' ) );

	/**
	 * Setup the WordPress core custom background feature.
	 */
	add_theme_support( 'custom-background', apply_filters( '_s_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // _s_setup
add_action( 'after_setup_theme', '_s_setup' );

/**
 * Register widgetized area and update sidebar with default widgets
 */
function _s_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', '_s' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', '_s_widgets_init' );

/**
 *Add featured image support
 */
add_theme_support( 'post-thumbnails' );


/**
 *Add thumbnail size
 */

add_image_size( 'calendar-thumb', 300, 214, true );


/**
 * Enqueue scripts and styles
 */
function _s_scripts() {
	wp_enqueue_style( '_s-style', get_stylesheet_uri() );

	wp_enqueue_script( '_s-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( '_s-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );
	wp_enqueue_script( '_s-modernizr', get_template_directory_uri() . '/library/js/vendor/custom.modernizr.js', array('jquery'), '1.0', true );
	wp_enqueue_script( '_s-jcycle2', get_template_directory_uri() . '/library/js/vendor/jquery.cycle2.min.js', array('jquery'), '20130801', true );
	wp_enqueue_script( '_s-jcycle2swipe', get_template_directory_uri() . '/library/js/vendor/jquery.cycle2.swipe.js', array('jquery'), '20130801', true );
	//wp_enqueue_script( '_s-sidr', get_template_directory_uri() . '/library/js/vendor/jquery.sidr.min.js', array('jquery'), '1.0', true );
	wp_enqueue_script( '_s-sidr', get_template_directory_uri() . '/library/js/vendor/jquery.jpanelmenu.min.js', array('jquery'), '1.0', true );
	/*Add Conditional for Venues Page*/
	wp_enqueue_script( '_s-scrolloroma', get_template_directory_uri() . '/library/js/vendor/jquery.superscrollorama.js', array('jquery'), '1.0', true );
	wp_enqueue_script( '_s-tweenmax', get_template_directory_uri() . '/library/js/vendor/TweenMax.min.js', array('jquery'), '1.9.7', true );
	//wp_enqueue_script( '_s-waypoint', get_template_directory_uri() . '/library/js/vendor/waypoints.min.js', array('jquery'), '2.0.3', true );
	wp_enqueue_script( '_s-modal', get_template_directory_uri() . '/library/js/vendor/jquery.reveal.js', array('jquery'), '1.4.4', true );
	/*End Venue Scripts*/

	wp_enqueue_script( '_s-app', get_template_directory_uri() . '/library/js/vendor/application.js', array('jquery'), '1.0', true );





	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	if ( is_singular() && wp_attachment_is_image() ) {
		wp_enqueue_script( '_s-keyboard-image-navigation', get_template_directory_uri() . '/js/keyboard-image-navigation.js', array( 'jquery' ), '20120202' );
	}
}
add_action( 'wp_enqueue_scripts', '_s_scripts' );

/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
* Royal Slider Fuzz Skin
*/
add_filter('new_royalslider_skins', 'new_royalslider_add_custom_skin', 10, 2);
function new_royalslider_add_custom_skin($skins) {
      $skins['rsFuzz'] = array(
           'label' => 'Royal Fuzz',
           'path' => get_stylesheet_directory_uri() . '/library/royalslider/fuzz/rs-fuzz.css'  // get_stylesheet_directory_uri returns path to your theme folder
      );
      return $skins;
}

add_filter('upload_mimes', 'custom_upload_mimes');
 
function custom_upload_mimes ( $existing_mimes=array() ) {
 
    // change the word forbiddenfiletype below to an extension you wish to allow
     $existing_mimes['forbiddenfiletype'] = 'mime/type';
    
    // call the modified list of extensions
    return $existing_mimes;
}
