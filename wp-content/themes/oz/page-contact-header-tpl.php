<?php
/**
 * Template Name: Contact w Header 
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'contact' ); ?>


			<?php endwhile; // end of the loop. ?>

	</div><!-- #primary -->

<?php get_footer(); ?>
