<?php
/**
 * The template used for displaying page content in page-patronage-tpl.php
 *
 * @package _s
 */
?>
<style>
	.attachment-post-thumbnail {
		width: 100%;
	}
</style>
<?php 
if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
  the_post_thumbnail();
} 
?>
<div class="patronage-contain">
	<div class="row">
		<div class="large-12 large-centered columns">
			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<header class="entry-header">
					<h1 class="entry-title"><?php the_title(); ?></h1>
				</header><!-- .entry-header -->

				<div class="entry-content">
					<?php the_content(); ?>

				</div><!-- .entry-content -->
			</article><!-- #post-## -->
		</div>
	</div>
</div>
