<?php
/**
 * Template Name: Front Page
 *
 * @package _s
 */

get_header(); ?>

	<section class = "home-slideshow full-width">
		<img class="home-logo" src="<?php echo get_template_directory_uri(); ?>/library/images/oz_home_logo.png" alt="OZ Nashville Arts">
		<!--<div class="cycle-slideshow" 
						    data-cycle-fx=fade
						    data-cycle-timeout=5000
						    data-cycle-swipe=true
						    data-cycle-loader="wait"
						    data-cycle-slides="> li"
						    >
			<div class="cycle-prev"></div>
   			<div class="cycle-next"></div>		
			<div class="cycle-pager"></div>
			 <li><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_GrandSalon_lit.jpg" alt="Grand Salon"></li>
		   	 <li><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_BraveNewArt.jpg" alt="Brave New Art"></li>
		   	 <li><a href="/friendslaunch"><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_Programs1.jpg" alt="Programs"></a></li>
		   	 <li><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_Terrace.jpg" alt="Terrace"></li>
		   	 <li><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_ZenGarden.jpg" alt="Zen Garden"></li>
		   	 <li><img src="http://localhost:8888/wp-content/uploads/2013/09/OZNashville_GrandSalon.jpg" alt="Grand Salon"></li>
		</div>-->
		<?php echo get_new_royalslider(9); ?>
	</section>

	<div class="top-nav-contain">
		<div class="row">
	
			<div class="logo-contain large-2 columns">
				<nav class="burger">
					<a id="sidr-trig" href="#" title="access main nav" class="burger-icon"><img src="<?php echo get_template_directory_uri(); ?>/library/images/burger.svg" alt="Acess Main Nav"></a>
					<!--<a id="sidr-trig" href="#" title="access main nav" class="burger-icon">&#9776;</a>-->
				</nav>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="/wp-content/themes/oz/library/images/logo_oz_black.jpg" alt="Oz Nashville Arts Museum" /></a>
			</div>
			<nav class= "columns large-10 primary-nav">
				<?php wp_nav_menu( array('menu' => 'Primary' )); ?>
			</nav>
		</div>
		
		
	</div>	
	<?php while ( have_posts() ) : the_post(); ?>

	<section class= "home-intro">
		<div class="row">
			<div class="columns large-8 large-centered">
				<h1><?php the_title(); ?></h1>
			
				<?php the_content(); ?>
			</div>
		</div>
	</section>
	<?php endwhile; ?>

	<section class="home-events">
		<a href="/programs" class="columns large-6 home-upcoming home-event">
			<img src="/wp-content/themes/oz/library/images/updated_programs.jpg" alt="Oz Nashville Arts Museum" />
			<div class="bucket-caption orange-overlay">
			</div>
			<span class="inline-block orange-cta cta" href="/programs">Program</span>
		</a><!--.home-upcoming-->

		<a href="/venue-page" class="columns large-6 home-recent home-event">
			<img src="/wp-content/themes/oz/library/images/OZNashville_venues.jpg" alt="Oz Nashville Arts Museum" />
			<div class="bucket-caption blue-overlay">
			</div>
			<span class="inline-block blue-cta cta large-dev" href="/venue">Venue</span>
			<span class="inline-block blue-cta cta small-dev" href="/venue-small-device">Venue</span>
		</a><!--.home-recent-->		
	</section>
	
<?php get_footer(); ?>
