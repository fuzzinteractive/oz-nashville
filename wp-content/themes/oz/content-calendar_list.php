<?php
/**
 * @package _s
 */
?>

	
		<dt class="cal-list-title row full-row <?php the_field('background_color'); ?>">

			<div class="date-contain">
				<span class="cal-list-month">
					<?php the_field('month'); ?>
				</span>
				<span class="cal-list-day">
					<?php the_field('display_day'); ?>
				</span>
			</div>

			<a href=""><?php the_title(); ?></a>

		</dt>
		<?php $post_thumbnail_id = get_post_thumbnail_id();
		$post_thumbnail_url = wp_get_attachment_url( $post_thumbnail_id ); ?>
		<?php if($post_thumbnail_url){ ?>
			<dd style="background: url(<?php echo $post_thumbnail_url; ?>) no-repeat center; background-size: cover;">
		<?php } else { ?>
			<dd>
			<?php } ?>
			<div class="row full-row cal-list-info">
				<a class="cal-list-info-btn" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">Info / Tickets</a>
			</div>
		</dd>


