<?php
/**
 * Template Name: Venue Page
 *
 * different template.
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<!-- OVERVIEW hardcoded - to be replaced with a wp page -->
		<div class="venue overview">
			<div class="row">
				<div class="columns large-8">
					<h1 class="entry-title">OVERVIEW</h1>
					<nav class="venue-sub"><a class="pink-text" href="#">Special Events</a> / <a class="blue-text" href="#">Weddings</a> / <a class ="orange-text" href="#">Corporate</a></nav>
					<p>OZ features five distinctly unique areas for truly memorable occasions.  From the dramatic Ultra Lounge Patio with retractable glass doors, majestic views, and romantic fire pit area, to the intimately peaceful Zen Garden with exotic foliage–OZ contains one-of-a-kind spaces to create unforgettable moments.  The venue also boasts one of the world’s largest walk-in cigar humidors, The Escaparate, which held over 100,000 premium cigars.  A 12,000 square-foot warehouse space provides a sizable canvas to create the original environment of your choosing.</p>
					<p>OZ prides itself on its staff of creative, professional, and customer-friendly people.  Our team will provide you with attentive, one-on-one service and has a variety of professional resources that will be at your disposal.  Our goal is to assist you in creating the occasion of your dreams and one you will cherish for the rest of your life.</p>
					<p><a href="/contact">Contact u</a> to arrange a private tour to view the space and imagine the possibilities</p>
					<p class="venue-scroll-instr">SCROLL TO VIEW ROOMS</p>
				</div>
			</div>
		</div>




			<?php $args = array( 'post_type' => 'venues', 'posts_per_page' => 10, 'order'=> 'asc' );
			$loop = new WP_Query( $args );
			$i = 0;
			while ( $loop->have_posts() ) : $loop->the_post();
				$i++;
				if($i == 1 || $i == 5) {
					$color = "green";
				} elseif($i == 2 || $i == 6) {
					$color = "blue";
				}
				elseif($i == 3 || $i == 7) {
					$color = "orange";
				}
				else {
					$color = "pink";
				} ?>

				<div class="venue <?php echo $color; ?>" id="venue-<?php echo $i ?>">

					<!--ADD Background image -->
					<div class='venue-header' style="background-image: url('<?php the_field('header-image'); ?>"></div>

					<div class="venue-title-contain">
						<div class="row">
							<div class="columns large-5 end venue-text">
								<div class="d-bkg"></div>
								<h1 class="venue-title"><?php the_title(); ?></h1>
							</div>
						</div>
					</div>

					
					<div class="venue-info-contain" data-anchor="anchor-<?php echo $i; ?>">
						<div class="row full-row">
							<div class="large-4 large-offset-1 columns venue-text">
								<h2 class="v-title"><?php the_title(); ?></h2>

								<!--Display if floorplan-->
								<a class="floor-plan">View Floor Plan &raquo;</a>
								<?php the_content(); ?>
							</div>

							<div class="large-6 columns">
								<div class="cycle-slideshow" 
								    data-cycle-fx=scrollHorz
								    data-cycle-timeout=5000
								    data-cycle-pager=".pager-<?php echo $i; ?>"
								    >
							
								    <div class="cycle-pager"></div>
								    <?php $images = get_field('venue_gallery'); 
								     foreach( $images as $image ): ?>
								   	 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								 	<?php endforeach; ?>
								</div>
								<div class="pager-<?php echo $i; ?>"></div>

							</div>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
	</div><!-- #primary -->

	<nav class="venue-path">
		<li><a data-scroll="top" href="">Overview</a></li>
		<li><a data-scroll="anchor-1" href="">Ozgener Gallery</a></li>
		<li><a data-scroll="anchor-2" href="">Grand Salon</a></li>
		<li><a data-scroll="anchor-3" href="">Ultra Lounge</a></li>
		<li><a data-scroll="anchor-4" href="">Patio Contempo</a></li>
		<li><a data-scroll="anchor-5" href="">Escaparten</a></li>
		<li><a data-scroll="anchor-6" href="">Zen Garden</a></li>
		<li><a data-scroll="anchor-7" href="">Boardroom</a></li>
	</nav>

	<script>
		jQuery(document).ready(function(){
			jQuery('.venue-path a').on('click', function() {

    var scrollAnchor = jQuery(this).attr('data-scroll'),
        scrollPoint = jQuery('div[data-anchor="' + scrollAnchor + '"]').offset().top - 28;

    jQuery('body,html').animate({
        scrollTop: scrollPoint
    }, 500);

    return false;

})


jQuery(window).scroll(function() {
    var windscroll = jQuery(window).scrollTop();
    if (windscroll >= 100) {
        jQuery('.venue-path').addClass('fixed');
        jQuery('.content-area .venue').each(function(i) {
            if (jQuery(this).position().top <= windscroll - 20) {
                jQuery('.venue-path a.active').removeClass('active');
                jQuery('.venue-path a').eq(i).addClass('active');
            }
        });

    } else {

        jQuery('.venue-path').removeClass('fixed');
        jQuery('.venue-path a.active').removeClass('active');
        jQuery('.venue-path a:first').addClass('active');
    }

}).scroll();
		})
	</script>
<?php get_footer(); ?>
