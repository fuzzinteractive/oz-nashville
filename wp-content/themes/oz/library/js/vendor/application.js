//Home Page Sticky Nav

jQuery(window).scroll(function() {    
    var scroll = jQuery(window).scrollTop();
    if (scroll >= jQuery('.home-slideshow').outerHeight()) {     
        jQuery(".home .top-nav-contain").addClass("fix-me");
    }
    else {
    	 jQuery(".home .top-nav-contain").removeClass("fix-me");
    }
}); 


//Inside Sticky Nav
if(!jQuery('body').hasClass('home')) {
	jQuery(window).scroll(function() {    
	    var scroll = jQuery(window).scrollTop();
	    if (scroll >= 300) {     
	        jQuery(".top-nav-contain").addClass("slide-up");
	       
	    }
	    else {
	    	 jQuery(".top-nav-contain").removeClass("slide-up");
	    	
	    }
	}); 
}


jQuery(document).ready(function(){

	jQuery('.site-wrap').addClass('show-wrap');




	var jPM = jQuery.jPanelMenu({
		menu: '#sidr',
		trigger: '#sidr-trig'
	});
	jPM.on();
	//Sidr Nav
	//jQuery('#sidr-trig').sidr();	
	//About Page Accordian courtesy css-tricks.com


	


	var allPanels = jQuery('.accordion > dd').hide();
    jQuery('.accordion dd.land-open').show();
  	jQuery('.accordion > dt > a').click(function() {

  	jQuery('.accordion dd.land-open').removeClass('land-open');
    jQuerythis = jQuery(this);
    jQuerytitle = jQuerythis.parent();
    jQuerytarget =  jQuerythis.parent().next();
	  if(!jQuerytarget.hasClass('active')){
	     allPanels.removeClass('active').slideUp();
	     jQuerytarget.addClass('active').slideDown();
	  }
	  jQuery('.accordion dt').removeClass('title-on');
	  jQuerytitle.addClass('title-on');
      
    return false;

    jQuery('ven-path').css({'left': ''});
    
  });
//Smooth Scrolling
	jQuery(function() {
	  jQuery('a[href*=#]:not([href=#])').click(function() {
	    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
	        || location.hostname == this.hostname) {

	      var target = jQuery(this.hash);
	      target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
	      if (target.length) {
	        jQuery('html,body').animate({
	          scrollTop: target.offset().top
	        }, 1000);
	        return false;
	      }
	    }
	  });
	});

  //Venue Script

if (jQuery(window).width() > 1025) { 

	var windowHeight = jQuery(window).height(); 
//	jQuery(window).bind("resize", methodToFixLayout);
//	function methodToFixLayout(e){
//			var windowHeight = jQuery(window).height(); 
//		}
	// Set all venue items to have content 'below fold' to allow for just background image to be shown
	//jQuery('.venue.overview').css({'min-height': windowHeight+'px'}); 
	jQuery('.venue-header').css({height: windowHeight+'px'});
	jQuery('.venue-title-contain').css({height: windowHeight+400+'px'});	
	//jQuery('.venue-info-contain').css({height: windowHeight+100+'px'});


	var controller = jQuery.superscrollorama();
	controller.addTween('#venue-0 .venue-text', 
	    TweenMax.from(jQuery('#venue-0 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-0 .venue-title-contain', 900);
	controller.addTween('#venue-1 .venue-text', 
	    TweenMax.from(jQuery('#venue-1 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-1 .venue-title-contain', 900);
	controller.addTween('#venue-2 .venue-text', 
	    TweenMax.from(jQuery('#venue-2 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-2 .venue-title-contain', 900);
	controller.addTween('#venue-3 .venue-text', 
	    TweenMax.from(jQuery('#venue-3 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-3 .venue-title-contain', 900);
	controller.addTween('#venue-4 .venue-text', 
	    TweenMax.from(jQuery('#venue-4 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-4 .venue-title-contain', 900);
	controller.addTween('#venue-5 .venue-text', 
	    TweenMax.from(jQuery('#venue-5 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-5 .venue-title-contain', 900);
	controller.addTween('#venue-6 .venue-text', 
	    TweenMax.from(jQuery('#venue-6 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-6 .venue-title-contain', 900);
	controller.addTween('#venue-7 .venue-text', 
	    TweenMax.from(jQuery('#venue-7 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-7 .venue-title-contain', 900);
	controller.addTween('#venue-8 .venue-text', 
	    TweenMax.from(jQuery('#venue-8 .venue-text'), .1, {css:{opacity:0}}));
	//controller.pin('#venue-8 .venue-title-contain', 900); 

}




//Program Event Single 


	jQuery('.buy-tix-btn').on('click', function(){
		jQuery('.buy-ticket-contain').slideToggle();
	});


	jQuery('.read-more').toggle(function(e){
		jQuery(this).html('less &and;');
		e.preventDefault();
		var totalHeight = jQuery('.prog-content-contain').prop('scrollHeight');
		console.log(totalHeight);
		jQuery('.prog-content-contain').animate({height: totalHeight+100}, 500);
	},
		function(e){
			jQuery(this).html('read more &or;');
		e.preventDefault();
		jQuery('.prog-content-contain').animate({height: 500}, 500);
	});


	jQuery('.pro-more-info').on('click', function(){
		jQuery('.pro-more-info-contain').fadeOut('slow');
	});


//Support Page

	jQuery('.support-trigger').on('click', function(e){
		e.preventDefault();
		jQuery('#support-buy').slideToggle();
	});	

	jQuery('.volunteer-trigger').on('click', function(e){
		e.preventDefault();
		jQuery('#mc_embed_signup').slideToggle();
	});	



});


jQuery(window).scroll(function() { 
	if(jQuery(window).scrollTop() > 10){
      jQuery('.pro-more-info-contain').fadeOut(1000);
    }

});