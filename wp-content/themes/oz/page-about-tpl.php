<?php
/**
 * Template Name: About Page
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">
			<?php while ( have_posts() ) : the_post(); ?>

				<dl class="accordion">

					<dt class="title-on title-0"><a href=""><?php the_title(); ?></a></dt>
					<dd class="land-open">
						<div class="row">
							<div class="large-10 columns large-centered">
								<?php the_content(); ?>
							</div>
						</div>
					</dd>
					

					<dt class="title-1"><a href="">Vision &amp; Mission</a></dt>
					<dd>
						<div class="row">
							<div class="large-10 columns large-centered">
								<?php the_field('vision_and_mission'); ?>
							</div>
						</div>
					</dd>

					<dt class="title-2"><a href="">HISTORY</a></dt>
					<dd>
						<div class="row">
							<div class="large-10 columns large-centered">
								<?php the_field('history_content'); ?>
							</div>
						</div>
					</dd>


					<dt class="title-3"><a href="">SUPPORTERS</a></dt>
					<dd>
						<div class="row support">
							<div class="large-3 columns  board-staff board-staff-1-contain">
								<?php the_field('supporters'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-2-contain">
								<?php the_field('supporters_col_2'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-3-contain">
								<?php the_field('supporters_col_3'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-4-contain">
								<?php the_field('supporters_col_4'); ?>
							</div>
						</div>
					</dd>

					<dt class="title-4"><a href="">STAFF</a></dt>
					<dd>
						<div class="row staff">
							<div class="large-3 columns  board-staff board-staff-1-contain">
								<?php the_field('board_and_staff'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-2-contain">
								<?php the_field('board_and_staff_col_2'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-3-contain">
								<?php the_field('board_and_staff_col_3'); ?>
							</div>
							<div class="large-3 columns  board-staff board-staff-4-contain">
								<?php the_field('board_and_staff_col_4'); ?>
							</div>
						</div>
					</dd>

				</dl>


			<?php endwhile; // end of the loop. ?>

	</div><!-- #primary -->

<?php get_footer(); ?>