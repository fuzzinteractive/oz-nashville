<?php
/**
 * @package _s
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="pro-top">
		<div class="pro-more-info-contain">
			<a href="#program-info"><button class="green-btn pro-more-info">More Info / Tickets &or;</button></a>
		</div>
		<?php the_content(); ?>
	</div>
		<div class="row" id="program-info">
			<a class="back-btn" href="/programs" title="back to calendar">&laquo; Back</a>
			<div class="share-this-contain large-6 columns">
				<div class= "prog-content-contain">
					<?php the_field('program_description'); ?>
					<a class="read-more">Read More &or;</a>
				</div>
				<div class="share-icon-contain">
					<span class="sm-gray-title">Share:</span>
					<span class='st_facebook_large' displayText='Facebook'></span>
					<span class='st_twitter_large' displayText='Tweet'></span>
					<span class='st_pinterest_large' displayText='Pinterest'></span>
					<span class='st_googleplus_large' displayText='Google +'></span>
					<span class='st_email_large' displayText='Email'></span>
				</div><!--icon-contain-->
				
			</div>
			<div class="large-6 columns prog-info-contain">
				<?php the_field('info_box'); ?>
				<button class="green-btn buy-tix-btn">Buy Tickets</button>
				<div class="buy-ticket-contain">
				<?php the_field('ticket_embed_code'); ?>
				</div>

			</div>
		</div>


</article><!-- #post-## -->
