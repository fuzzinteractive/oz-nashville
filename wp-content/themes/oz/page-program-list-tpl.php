<?php
/**
 * Template Name: Program List View
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<div class="row full-row title-contain">
			<div class="large-6 columns">
				<h1 class="dots">CALENDAR</h1>
			</div>
			<nav class="large-2 columns view-switch">
				<span class="icon-contain">
					<a href="/programs">
						<svg version="1.1" class="nav-icon" id="grid-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
						<rect fill="#ffffff" width="27" height="27"/>
						<rect x="37" fill="#ffffff" width="26" height="27"/>
						<rect x="73" fill="#ffffff" width="27" height="27"/>
						<rect y="37" fill="#ffffff" width="27" height="26"/>
						<rect x="37" y="37" fill="#ffffff" width="26" height="26"/>
						<rect x="73" y="37" fill="#ffffff" width="27" height="26"/>
						<rect y="73" fill="#ffffff" width="27" height="27"/>
						<rect x="37" y="73" fill="#ffffff" width="26" height="27"/>
						<rect x="73" y="73" fill="#ffffff" width="27" height="27"/>
						</svg>
					</a>
				</span>
				<span class="icon-contain">
					<a href="/program-list">
						<svg version="1.1" class="nav-icon" id="list-icon" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
							 width="100px" height="100px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
						<rect fill="#ffffff" width="27" height="27"/>
						<rect x="37" fill="#ffffff" width="50" height="27"/>
						<rect x="73" fill="#ffffff" width="27" height="27"/>
						<rect y="37" fill="#ffffff" width="27" height="26"/>
						<rect x="37" y="37" fill="#ffffff" width="55" height="26"/>
						<rect x="73" y="37" fill="#ffffff" width="27" height="26"/>
						<rect y="73" fill="#ffffff" width="27" height="27"/>
						<rect x="37" y="73" fill="#ffffff" width="53" height="27"/>
						<rect x="73" y="73" fill="#ffffff" width="27" height="27"/>
						</svg>
					</a>
				</span>
			</nav>
		</div>

		
		<div class="accordion calendar-list-accordion">
			<?php $args = array( 'post_type' => 'programs', 'posts_per_page' => 50 );
			$loop = new WP_Query( $args );
			while ( $loop->have_posts() ) : $loop->the_post(); ?>
			<?php get_template_part( 'content', 'calendar_list' ); ?>
			<?php endwhile; ?>
		</div>

	</div><!-- #primary -->

<?php get_footer(); ?>
