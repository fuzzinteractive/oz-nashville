<?php
/**
 * Template Name: Venue Page
 *
 * different template.
 *
 * @package _s
 */

get_header(); ?>

	<div id="primary" class="content-area">

		<!-- OVERVIEW hardcoded - to be replaced with a wp page -->
		<div class="venue overview">
			<div class="row">
				<div class="columns large-8 large-centered">
					<h1 class="entry-title">OVERVIEW</h1>
					<!--<nav class="venue-sub"><a class="pink-text" href="#">Special Events</a> / <a class="blue-text" href="#">Weddings</a> / <a class ="orange-text" href="#">Corporate</a></nav>-->
					<?php while ( have_posts() ) : the_post(); ?>

						<?php the_content(); ?>


					<?php endwhile; // end of the loop. ?>
					<?php wp_reset_postdata(); ?>
				</div>
			</div>
		</div>
		<img class="scroll-guy" src="<?php echo get_template_directory_uri(); ?>/library/images/scroll-guy.png" alt="scroll down">




			<?php $args = array( 'post_type' => 'venues', 'posts_per_page' => 10, 'order'=> 'asc', 'posts_not_in' => array(204,206,208) );
			$loop = new WP_Query( $args );
			$i = 0;
			while ( $loop->have_posts() ) : $loop->the_post();
				$i++;
				if($i == 1 || $i == 5) {
					$color = "green";
					$dmd ="green-dmd";
				} elseif($i == 2 || $i == 6) {
					$color = "blue";
					$dmd ="blue-dmd";
				}
				elseif($i == 3 || $i == 7) {
					$color = "orange";
					$dmd ="orange-dmd";
				}
				else {
					$color = "pink";
					$dmd ="pink-dmd";
				} ?>

				<div class="venue <?php echo $color; ?>" id="venue-<?php echo $i ?>" style="background: url('<?php the_field('header_image'); ?>') no-repeat center center fixed;  -webkit-background-size: cover;
  -moz-background-size: cover;
  -o-background-size: cover;
  background-size: cover;">

					<!--ADD Background image -->
					<div class='venue-header'></div>

					<div class="venue-title-contain">
						<div class="row full-row">
							<div class="large-11 large-offset-1 end venue-text">
								<?php $typetitle = get_field('title_image'); ?>
								<img class="venue-type-title" src="<?php echo $typetitle; ?>" alt="<?php the_title(); ?>">
								<!--<img class="dmd-bkg" src="<?php echo get_template_directory_uri(); ?>/library/images/<?php echo $dmd; ?>.png">-->
								<!--<div class="d-bkg"></div>-->
								<!--<h1 class="venue-title"><?php the_title(); ?></h1>-->
							</div>
						</div>
					</div>

					
					<div class="venue-info-contain" id="anchor-<?php echo $i; ?>">
						<div class="row full-row">
							<div class="large-4 columns scroll-control">
								<div class="venue-info-box">
									<h2 class="v-title"><?php the_title(); ?></h2>

									<!--Display if floorplan-->
									<a class="floor-plan overlink-<?php echo $i; ?>" href="#">View Floor Plan &raquo;</a>
									<?php the_content(); ?>

								</div>
							
							</div>

							<div class="large-8 slide-contain columns">
								<div class="cycle-slideshow" 
								    data-cycle-fx=scrollHorz
								    data-cycle-timeout=5000
								    data-cycle-paused=true
								    data-cycle-pager=".pager-<?php echo $i; ?>"
								    >
							
								    <div class="cycle-pager"></div>
								    <?php $images = get_field('venue_gallery'); 
								     foreach( $images as $image ): ?>
								   	 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
								 	<?php endforeach; ?>
								</div>
								<div class="pager-<?php echo $i; ?> example-pager"></div>

							</div>
						</div>
						<div class="fader"></div>
					</div>
					<div class="over-<?php echo $i; ?> oz-overlay reveal-modal xlarge">
						<span class="close-reveal-modal close">&#215;</span>
						<span class='prev-oz'><img src="/wp-content/themes/oz/library/images/nav-prev.png" alt="previous"></span>
        				<span class='next-oz'><img src="/wp-content/themes/oz/library/images/nav-next.png" alt="next"></span>
						<div class="cycle-slideshow" 
						    data-cycle-fx=scrollHorz
						    data-cycle-timeout=5000
						    data-cycle-prev=".prev-oz"
        					 data-cycle-next=".next-oz"
						    data-cycle-pager=".floor-pager-<?php echo $i; ?>"
						    >
	
						    <div class="floor-pager-<?php echo $i; ?> example-pager"></div>
						    <?php $images = get_field('floor_plan'); 
						     foreach( $images as $image ): ?>
						   	 <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
						 	<?php endforeach; ?>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
	
	</div><!-- #primary -->

	<nav class="ven-path">
		<li><a  href="#top">Overview</a></li>
		<li><a  href="#anchor-1">Ozgener Gallery</a></li>
		<li><a  href="#anchor-2">Grand Salon</a></li>
		<li><a href="#anchor-3">Ultra Lounge</a></li>
		<li><a  href="#anchor-4">Veranda</a></li>
		<li><a  href="#anchor-5">Escaparate</a></li>
		<li><a  href="#anchor-6">Zen Garden</a></li>
		<li><a  href="#anchor-7">Boardroom</a></li>
	</nav>

	<script>

	jQuery(document).ready(function(){
		//jQuery('.ven-path')css("left", "");

      

      
           jQuery('.overlink-2').click(function(e) {        // Button which will activate our modal
            jQuery('.over-2').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });
            jQuery('.overlink-3').click(function(e) {        // Button which will activate our modal
            jQuery('.over-3').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });
             jQuery('.overlink-4').click(function(e) {        // Button which will activate our modal
            jQuery('.over-4').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });
              jQuery('.overlink-5').click(function(e) {        // Button which will activate our modal
            jQuery('.over-5').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });
               jQuery('.overlink-6').click(function(e) {        // Button which will activate our modal
            jQuery('.over-6').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });
                jQuery('.overlink-7').click(function(e) {        // Button which will activate our modal
            jQuery('.over-7').reveal({                // The item which will be opened with reveal
                animation: 'fade',              // fade, fadeAndPop, none
                animationspeed: 600,            // how fast animtions are
                closeonbackgroundclick: true,   // if you click background will modal close?
                dismissmodalclass: 'close'      // the class of a button or element that will close an open modal
            });
        return false;
        });

jQuery(window).scroll(function() {
    var windscroll = jQuery(window).scrollTop();
    if (windscroll >= 100) {
        jQuery('.content-area .venue').each(function(i) {
            if (jQuery(this).position().top <= windscroll - 20) {
                jQuery('.ven-path li.active').removeClass('active');
                jQuery('.ven-path li').eq(i).addClass('active');
            }
        });

    } else {

        jQuery('.venue-path li.active').removeClass('active');
        jQuery('.venue-path li:first').addClass('active');
    }

}).scroll();
		})
	</script>
<?php get_footer(); ?>
